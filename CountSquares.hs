{-# LANGUAGE BangPatterns #-}
-- | Given a list of points in 2D space, count how many squares that can be
-- generated using points from the list. There are no repeats in the list. Note
-- that the squares need not be axis-aligned.
--
-- Author: Vladimir Komendantskiy
--
-- Date: 23-12-2013

module CountSquares
       (
         -- * Basic geometric types and operations
         Point(..)
       , DVector(..)
       , Vector(..)
       , vector
       , rotateLeft
       , rotateRight
       , shiftBy
         -- * Classification tools
       , UnitGrids
       , AssignVectors
       , assignVectors
         -- * Counting functions
       , CountUnitSquares
       , countUnitSquares
       , findEdgePairs
       , countInGrids
         -- ** The main counting function
       , countSquares
       )
  where

import Control.Monad.State (State, get, put, runState)
import Control.Parallel.Strategies (NFData, using, parTraversable, rdeepseq)
import Data.Bits (rotate)
import Data.List (sort, tails)
import Data.Hashable (Hashable, hash, hashWithSalt)
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet

newtype Point = Point (Rational, Rational)
              deriving (Show, Eq)

instance Hashable Point where
  hash (Point (x, y)) = (hash x `rotate` 4) + (hash y `rotate` (-2))
  {-# INLINE hash #-}
  hashWithSalt s (Point (x, y)) = (hashWithSalt s x `rotate` 4) +
                                  (hashWithSalt s y `rotate` (-2))
  {-# INLINE hashWithSalt #-}

instance NFData Point

-- | The ordering relation on points stipulates that a point (x,y) is less than
-- another, non-equal point (u,v) if and only if the angle c between the segment
-- ((x,y),(u,v)) and the half-open line ((x,y),(+inf,y)) is greater or equal to
-- 0 degrees and less than 180 degrees.
instance Ord Point where
  compare p q                               | p  == q             = EQ
  compare (Point (x1, y1)) (Point (x2, y2)) | y1 <  y2            = LT
                                            | y1 == y2 && x1 < x2 = LT
                                            | otherwise           = GT
  {-# INLINE compare #-}

-- | Type of displaced vector (o, v), a vector v that starts at a point o rather
-- than at the origin of coordinates.
newtype DVector = DVector (Point, Point)
                deriving Eq

instance Hashable DVector where
  hash (DVector (p, q)) = (hash p `rotate` (-3)) + (hash q `rotate` 6)
  {-# INLINE hash #-}
  hashWithSalt s (DVector (p, q)) = (hashWithSalt s p `rotate` (-3)) +
                                    (hashWithSalt s q `rotate` 6)
  {-# INLINE hashWithSalt #-}

instance NFData DVector

-- | A vector starts at the beginning of coordinates and points at p,
--
-- >  p  =  (l * cos c, l * sin c)
--
-- where l is the length of a vector and c is the angle of the vector. The
-- vector is represented by the point p only.
newtype Vector = Vector (Rational, Rational)
               deriving Eq

instance Hashable Vector where
  hash (Vector (x, y)) = (hash x `rotate` 7) + (hash y `rotate` (-5))
  {-# INLINE hash #-}
  hashWithSalt s (Vector (x, y)) = (hashWithSalt s x `rotate` 7) + (hashWithSalt s y `rotate` (-5))
  {-# INLINE hashWithSalt #-}

instance NFData Vector

-- | Replaces the vector at the coordinate origin.
vector :: DVector -> Vector
vector (DVector (Point (x1, y1), Point (x2, y2))) = Vector (x2 - x1, y2 - y1)
{-# INLINE vector #-}

-- | Rotates a vector 90 degrees counter-clockwise.
rotateLeft :: Vector -> Vector
rotateLeft (Vector (x, y)) = Vector (-y, x)
{-# INLINE rotateLeft #-}

-- | Rotates a vector 90 degrees clockwise.
rotateRight :: Vector -> Vector
rotateRight (Vector (x, y)) = Vector (y, -x)
{-# INLINE rotateRight #-}

-- | Shifts a displaced vector by a given absolute vector.
shiftBy :: DVector -> Vector -> DVector
shiftBy (DVector (Point (x1, y1), Point (x2, y2))) (Vector (x, y)) =
  DVector (Point (x1 + x, y1 + y), Point (x2 + x, y2 + y))
{-# INLINE shiftBy #-}

-- | A unit grid is a set of parallel codirected displaced vectors of equal
-- length. 'UnitGrids' is a mapping from vectors to sets of displaced
-- vectors. The \"unit\" is meant to be the length of a vector. The length is
-- implicit from the point vector representation, and we don't have to compute
-- the length separately.
type UnitGrids = HashMap Vector (HashSet DVector)

-- | A state monad type for stateful functional computation of unit grids.
type AssignVectors = State UnitGrids ()

-- | Assigns vectors to a given list of displaced vectors and constructs unit
-- grids. The domain of the resulting 'UnitGrids' does not contain perpendicular
-- vectors which are redundant since any square can be represented by only one
-- pair of its opposite edges rather than by all the two possible pairs.
assignVectors :: [DVector] -> AssignVectors
assignVectors []        = return ()
assignVectors (!dv:dvs) = do
  !grids <- get
  let v@(Vector (!x, _)) = vector dv
      p = x > 0
  if p then
    return ()
  else
    put $ HashMap.insertWith HashSet.union v (HashSet.singleton dv) grids
  assignVectors dvs

-- | Type of state for counting squares in the same unit grid.
type CountUnitSquares =
  State (Vector, HashSet DVector, Integer) Integer
--       ^ classifying vector
--               ^ set of remaining segments
--                                ^ internal accumulator
--                                         ^ output of the traversal

-- | @countUnitSquares es@ finds all the pairs of opposite sides of squares for
-- all edges in @es@. The initial state 'CountUnitSquares' should contain data
-- to iterate on.
countUnitSquares :: [DVector] -> CountUnitSquares
countUnitSquares []        = get >>= \(_, _, !n) -> return n
countUnitSquares (!e : es) = do
  (!v, !dvs, !n) <- get
  let !ePairs = findEdgePairs v e dvs
  put (v, HashSet.delete e dvs, n + ePairs)
  countUnitSquares es

-- | @findEdgePairs v e dvs@ counts the number of edges in @dvs@ that are on
-- the opposite side of a square containing the edge @e@.
findEdgePairs :: Vector -> DVector -> HashSet DVector -> Integer
findEdgePairs !v !e dvs = rEdge + lEdge
  where
    rEdge = findBy rotateRight
    lEdge = findBy rotateLeft
    findBy f = coerce $ HashSet.member (e `shiftBy` f v) dvs
    coerce b = if b then 1 else 0

-- | Counts the number of squares in each unit grid.
countInGrids :: UnitGrids ->
                State (Vector, HashSet DVector, Integer)
                      (HashMap Vector Integer)
countInGrids =
  HashMap.traverseWithKey (\v dvs -> do
                              put (v, dvs, 0)
                              countUnitSquares $ HashSet.toList dvs)

-- | The main counting function. It takes a list of points and produces the
-- number of squares.
countSquares :: [Point] -> Integer
countSquares pts | length pts < 4 = 0
countSquares pts = HashMap.foldr (+) 0 counts
  where
    sortedPts      = sort pts
    dVectorBundles = (init sortedPts) `zip` (tail $ tails sortedPts)
    dVectors       = concatMap
                       (\(x, ys) -> map (\y -> DVector (x, y)) ys)
                       dVectorBundles
    unitGrids      = snd $ runState (assignVectors dVectors) HashMap.empty
    counts         = (fst $ runState (countInGrids unitGrids) initState)
                     `using`
                     parTraversable rdeepseq
    initState      = (Vector (0, 0), HashSet.empty, 0)
