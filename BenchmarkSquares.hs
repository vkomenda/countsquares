{-# LANGUAGE BangPatterns #-}
-- | A benchmark for the square counting algorithm.
--
-- Author: Vladimir Komendantskiy
--
-- Date: 23-12-2013

module Main where

import CountSquares
import Control.DeepSeq (force, deepseq)
import Control.Monad (forM_)
import Data.Time.Clock

-- | Incrementally counts squares in lists of points produced by 'grids'.
main :: IO ()
main = do
  putStrLn $ "\n"
          ++ "This benchmark counts numbers of all possible squares delimited "
          ++ "by equally spaced points on regular N by N grids of the kind\n"
          ++ "0)     1) .    2) . .    3) . . .    4) . . . .\n"
          ++ "                  . .       . . .       . . . .\n"
          ++ "                            . . .       . . . .\n"
          ++ "                                        . . . .  and so on\n"
          ++ "If counting is correct, the output should contain the "
          ++ "4-dimensional pyramidal numbers 0,0,1,6,20,50,105...\n"
          ++ "Some more of those numbers are listed at http://oeis.org/A002415"
          ++ "\n\nPrint <Enter> to continue."
  _ <- getLine
  start0 <- getCurrentTime
  forM_ (zip idxs grids) $ \(n, points) -> points `deepseq` do
    putStrLn $ ":- " ++ show n ++ " by " ++ show n ++ " case"
    start1 <- getCurrentTime
    let !c = force $ countSquares points
    end1 <- getCurrentTime
    putStrLn $ "Number of squares: " ++ show c
    putStrLn $ "Time:              " ++ show (diffUTCTime end1 start1)
  end0 <- getCurrentTime
  putStrLn $ "Total time:        " ++ show (diffUTCTime end0 start0)
  where
    idxs :: [Integer]
    idxs = [0..]

-- | Outputs a sequence of lists of points that starts from
--
-- >                                  (0,2) (1,2) (2,2)
-- >                  (0,1) (1,1)     (0,1) (1,1) (2,1)
-- > []  ,  (0,0)  ,  (0,0) (1,0)  ,  (0,0) (1,0) (2,0)  ,  ...
--
-- and so on, up to (69,69) in the top right corner, that is, a set of 4,900
-- points that has 2,000,425 squares in it.
grids :: [[Point]]
grids = scanl outside [] [0..69]
  where
    outside points n = points ++ [Point (x, n) | x <- [0..n]]
                              ++ [Point (n, y) | y <- [0..n-1]]
