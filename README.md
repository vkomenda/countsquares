CountSquares
============

/Given a list of points in 2D space, count how many squares that can be
generated using points from the list. There are no repeats in the list. Note
that the squares need not be axis-aligned./


Basics
------

The ideas behind the module CountSquares are

- classification of directed segments between every pair of points according to

  -- the angle of the segment with respect to the x-axis,

  -- the length of the segment.

- use of only one pair of segments to identify a square; the other possible pair
  is not used for search to save time and space.



Installation and running
------------------------

There is a straightforward in-place build procedure in Unix using `make` and
`ghc`. Just run

> make

Otherwise please run the build commands from the Makefile by hand, and then run
`BenchmarkSquares`.

An optional profiling command is also available in the Makefile, run

> make profileView

to obtain `BenchmarkSquares.ps` with a colourful heap picture. Note that timings
will include profiler overhead in that case.


Module documentation
--------------------

To build the HTML documentation, run

> make doc

and browse doc/index.html.


Build dependencies
------------------

The source compiles against the following GHC packages:

base
deepseq
hashable
mtl
parallel
time
unordered-containers



/Vladimir Komendantskiy/
23-12-2013