BUILD=ghc -O2 -Wall -rtsopts -threaded -auto-all -caf-all --make
CORES=2

all: benchmark

benchmark: build
	./BenchmarkSquares +RTS -N$(CORES)

build:
	$(BUILD) CountSquares
	$(BUILD) BenchmarkSquares

doc:
	haddock -o doc --html BenchmarkSquares

profileView: profile
	hp2ps -c BenchmarkSquares.hp

profile: build
	./BenchmarkSquares +RTS -h

clean:
	rm -f *.hi
	rm -f *.o
	rm -f BenchmarkSquares
	rm -f BenchmarkSquares.hp
	rm -f BenchmarkSquares.ps
	rm -fr doc
